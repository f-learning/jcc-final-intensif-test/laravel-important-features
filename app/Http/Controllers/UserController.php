<?php

namespace App\Http\Controllers;

use App\Models\User;
use DB;
use Faker\Factory as Faker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Storage;

class UserController extends Controller
{
    public function create()
    {
        try {
            DB::beginTransaction();
            $faker = Faker::create('id_ID');
            $user = new User();
            $user->name = $faker->name;
            $user->email = $faker->email;
            $user->password = $faker->password;
            $user->save();

            Redis::set('user' . $user->id, $user);
            DB::commit();
            return response()->json(["user" => $user], 200);
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function get($id)
    {
        $cached = Redis::get('user' . $id);
        if (isset($cached)) {
            $user = json_decode($cached, false);
            $from = 'cache';
        } else {
            $user = User::find($id);
            if (!$user) {
                $from = 'tidak ada';
                $user = 'User dengan id: ' . $id . ' tidak ditemukan';
            } else {
                $from = 'db';
                Redis::set('user' . $id, $user);
            }
        }
        return response()->json(["user" => $user, "from" => $from], 200);
    }

    public function delete($id)
    {
        $status = 'User tidak ditemukan';
        $user = User::find($id);
        if ($user) {
            $user->delete();
            Redis::del('user' . $id);
            $status = 'Berhasil hapus data dengan id ' . $id;
        }
        return response()->json(["status" => $status], 200);
    }

    public function photo(Request $request)
    {
        $user = User::find($request['id']);
        if (is_null($user)) {
            $response = [
                'message' => 'User tidak ditemukan.',
                'data' => null,
            ];
            return response()->json($response, 400);
        }

        $path = 'jcc-intensif/test';
        $file = $request->file('photo');
        $ext = $file->getClientOriginalName();
        $name = pathinfo($ext, PATHINFO_FILENAME);
        $name = str_replace(' ', '-', $name);
        $extension = $file->getClientOriginalExtension();
        $finalName = $name . '-' . time() . '.' . $extension;
        $uploadPath = Storage::disk('s3')->put($path . $finalName, file_get_contents($file));
        if (!$uploadPath) {
            $response = [
                'message' => 'Gagal mengunggah foto profil.',
                'data' => null,
            ];
            return response()->json($response, 400);
        }

        $oldPhoto = str_replace(env('AWS_URL'), '', $user->photo);
        $user->photo = $path . $finalName;
        if (!$user->save()) {
            Storage::disk('s3')->delete($uploadPath);
            $response = [
                'message' => 'Gagal mengubah foto profil.',
                'data' => null,
            ];
            return response()->json($response, 400);
        }

        Storage::disk('s3')->delete($oldPhoto);
        $response = [
            'message' => 'Success.',
            'data' => $user,
        ];
        return response()->json($response, 200);

    }
}
